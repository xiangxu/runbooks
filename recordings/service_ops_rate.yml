groups:
- name: GitLab Component Operations-per-Second Rates
  interval: 1m
  rules:
  # Postgres Service
  - record: gitlab_component_ops:rate
    labels:
      component: 'service'
    expr: >
      sum by (environment, tier, type) (rate(pg_stat_database_xact_commit{tier="db", type="postgres"}[1m]))
      +
      sum by (environment, tier, type) (rate(pg_stat_database_xact_rollback{tier="db", type="postgres"}[1m]))
  # Redis Service
  - record: gitlab_component_ops:rate
    labels:
      component: 'server'
    expr: >
      sum by (environment, tier, type) (redis_instantaneous_ops_per_sec)
  # Registry Service
  - record: gitlab_component_ops:rate
    labels:
      type: 'registry'
      component: 'server'
      tier: 'sv'
    expr: >
      sum by (environment) (haproxy_backend_current_session_rate{backend="registry"})
  # gitlab_shell
  - record: gitlab_component_ops:rate
    labels:
      tier: 'sv'
      type: 'git'
      component: 'gitlab_shell'
    expr: >
      sum by (environment) (haproxy_backend_current_session_rate{backend=~"ssh|altssh"})
  # web:workhorse
  - record: gitlab_component_ops:rate
    labels:
      component: 'workhorse'
    expr: >
      sum by (environment, tier, type) (rate(gitlab_workhorse_http_requests_total{job="gitlab-workhorse-web"}[1m]))
  # api:workhorse
  - record: gitlab_component_ops:rate
    labels:
      component: 'workhorse'
    expr: >
      sum by (environment, tier, type) (rate(gitlab_workhorse_http_requests_total{job="gitlab-workhorse-api"}[1m]))
  # api/web/sidekiq/git:unicorn
  - record: gitlab_component_ops:rate
    labels:
      component: 'unicorn'
    expr: >
      sum by (environment, tier, type) (rate(http_request_duration_seconds_count{job="gitlab-unicorn"}[1m]))
  # sidekiq
  - record: gitlab_component_ops:rate
    labels:
      component: 'sidekiq'
    expr: >
      sum by (environment, tier, type) (rate(gitlab_transaction_duration_seconds_count{type = "sidekiq"}[1m]))
  # mailroom
  # TODO: zero placeholder as we have no metrics for mailroom at present
  - record: gitlab_component_ops:rate
    labels:
      component: 'service'
    expr: >
      count by (environment, tier, type) (up{type="mailroom"}) - count by (environment, tier, type) (up{type="mailroom"})
  # pgbouncer
  - record: gitlab_component_ops:rate
    labels:
      component: 'service'
    expr: >
      sum by (environment, tier, type) (rate(pgbouncer_stats_queries_total[1m]))
  # gitaly:goserver
  - record: gitlab_component_ops:rate
    labels:
      component: 'goserver'
    expr: >
      sum by (environment, tier, type) (rate(grpc_server_started_total{type="gitaly"}[1m]))
  # pages
  - record: gitlab_component_ops:rate
    labels:
      type: 'pages'
      tier: 'sv'
      component: 'service'
    expr: >
      sum by (environment) (haproxy_backend_current_session_rate{backend=~"pages_.*"})
  # HAProxy
  - record: gitlab_component_ops:rate
    labels:
      type: 'haproxy'
      tier: 'lb'
    expr: >
      sum by (environment) (haproxy_frontend_current_sessions)

- name: GitLab Service Operations-per-Second Aggregated Rates
  interval: 1m
  rules:
  # Aggregate over all components within a service
  - record: gitlab_service_ops:rate
    expr: >
      sum by (environment, tier, type) (gitlab_component_ops:rate)

- name: GitLab Component Operations-per-Second Rate Stats
  interval: 5m
  rules:
  # Average values for each component, over a week
  - record: gitlab_component_ops:rate:avg_over_time_1w
    expr: >
      avg_over_time(gitlab_component_ops:rate[1w])
  # Stddev for each component, over a week
  - record: gitlab_component_ops:rate:stddev_over_time_1w
    expr: >
      stddev_over_time(gitlab_component_ops:rate[1w])

- name: GitLab Service Operations-per-Second Rate Stats
  interval: 5m
  rules:
  # Average values for each service, over a week
  - record: gitlab_service_ops:rate:avg_over_time_1w
    expr: >
      avg_over_time(gitlab_service_ops:rate[1w])
  # Stddev for each service, over a week
  - record: gitlab_service_ops:rate:stddev_over_time_1w
    expr: >
      stddev_over_time(gitlab_service_ops:rate[1w])
